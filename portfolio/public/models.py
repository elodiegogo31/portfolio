from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class UserProfile(models.Model):
	user = models.OneToOneField(User, on_delete=models.CASCADE)

	avatar = models.ImageField(upload_to='user_avatar', null=True, blank=True)
	tel = models.CharField(max_length=100, null=True)
	address = models.CharField(max_length=500, null=True)
	def __str__(self):
		return self.user.last_name
	
class Technologies(models.Model):
	name = models.CharField(max_length=300, unique=True)
	picture = models.ImageField(upload_to='tech_pic', null=True, blank=True)
	def __str__(self):
		return self.name

class Articles(models.Model):
	title = models.CharField(max_length=300, unique=True)
	slug = models.SlugField( db_index=True)
	content = models.TextField()
	picture = models.ImageField(upload_to='article_pic', null=True, blank=True)
	user = models.ForeignKey( UserProfile, on_delete=models.CASCADE)
	published = models.BooleanField(default=False)
	created_at = models.DateTimeField(auto_now_add=True)
	technologies = models.ManyToManyField(Technologies, related_name='articles', blank=True)
	def __str__(self):
		return self.title

class Projects(models.Model):
	name = models.CharField(max_length=300, unique=True)
	slug = models.SlugField( db_index=True)
	description = models.TextField()
	illustration =  models.ImageField(upload_to='project_illustration', null=True, blank=True)
	repo_url = models.URLField(null=True)
	website_url = models.URLField(null=True)
	created_at = models.DateTimeField(auto_now_add=True)
	published = models.BooleanField(default=False)
	user = models.ForeignKey( UserProfile, on_delete=models.CASCADE)
	technologies = models.ManyToManyField(Technologies, related_name='projects', blank=True)
	def __str__(self):
		return self.name


class Messages(models.Model):
	lastname = models.CharField(max_length=100)
	firstname = models.CharField(max_length=100)
	email = models.EmailField()
	content = models.TextField()
	read = models.BooleanField(default=False)
	def __str__(self):
		return self.content

class SiteSections(models.Model):
	name = models.CharField(max_length=200)
	picture =  models.ImageField(upload_to='section_pic', null=True, blank=True)
	path_to_administer = models.CharField(max_length=600)
	def __str__(self):
		return self.name