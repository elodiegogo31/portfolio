from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.contrib.auth.models import User
from .models import Technologies, Articles, Messages, Projects, SiteSections, UserProfile
from django.contrib.auth import authenticate, login
from django.contrib.auth import logout as django_logout
from .forms import UserForm, UserProfileForm, TechnologiesForm, ArticlesForm, ProjectsForm, MessagesForm, SiteSectionsForm
from django.contrib.auth.decorators import login_required
# Create your views here.

def index(request):
	return HttpResponse("Hello, world. You're at the polls index.")

def home(request):
	context= {
		"truc" : "muche"
	}
	return render(request, 'public/home.html', context)

def projects(request):
	#project_list= Projects.objects.filter(published=True)
	projects_list= Projects.objects.all().order_by('-created_at')  #en attendant

	context= {
		"projects" : projects_list
	}
	return render(request, 'public/projects.html', context)


def articles(request):
	articles_list= Articles.objects.all().order_by('-created_at')  #en attendant

	context= {
		"articles" : articles_list
	}
	return render(request, 'public/articles.html', context)

def contacts(request):
	if request.method == 'POST':
		contact_form = MessagesForm(request.POST)

		if contact_form.is_valid():

			newcontact = Messages.objects.create(firstname= contact_form.cleaned_data['firstname'], lastname= contact_form.cleaned_data['lastname'], email=contact_form.cleaned_data['email'], content =contact_form.cleaned_data['content'])

			newcontact.save()
			
			return render(request, 'public/contact.html', {'form':contact_form})

	else:
		contact_form = MessagesForm()
		return render(request, 'public/contact.html', {'form': contact_form})



def register(request):
    registered = False

    if request.method == 'POST':
        user_form = UserForm(data=request.POST)
        profile_form = UserProfileForm(data=request.POST)

        if user_form.is_valid() and profile_form.is_valid():
            user = user_form.save()
            user.set_password(user.password)
            user.save()
            profile = profile_form.save(commit=False)
            profile.user = user

            if 'avatar' in request.FILES:
                profile.avatar = request.FILES['avatar']
            
            profile.save()
            registered = True
        else:
            print(user_form.errors,profile_form.errors)
    else:
        user_form = UserForm()
        profile_form = UserProfileForm()
    return render(request,'public/registration.html',
                          {'user_form':user_form,
                           'profile_form':profile_form,
                           'registered':registered})

def user_login(request):

    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)

        if user:
            if user.is_active:
                login(request,user)
                return HttpResponse("sucess login")
            else:
                return HttpResponse("Your account was inactive.")
        else:
            print("Someone tried to login and failed.")
            print("They used username: {} and password: {}".format(username,password))
            return HttpResponse("Invalid login details given")
    else:
        return render(request, 'public/login.html', {})

@login_required
def dashboard(request):

	if request.method == 'POST':
		techno_form = TechnologiesForm(request.POST)
		print(techno_form)

		if techno_form.is_valid():
			print(techno_form.cleaned_data)

			newtech = Technologies.objects.create(name= techno_form.cleaned_data['name'], picture= None)

			newtech.save()
			print(newtech)
			if 'picture' in request.FILES:

				newtech.picture = request.FILES['picture']
				newtech.save()
				return redirect('technologiesAdmin')
			else:
				newtech.picture = "/tech_pic/indexsds.png"
				newtech.save()
			return redirect('technologiesAdmin')

	else:
		techno_form = TechnologiesForm()
	return render(request, 'public/dashboard.html', {'form': techno_form})


@login_required
def articlesCreation(request):

	if request.method == 'POST':
		articles_form = ArticlesForm(request.POST)
		print(articles_form)
		user = request.POST.get('user')
		print(user)

		if articles_form.is_valid():
			print(articles_form.cleaned_data)
			print(articles_form.cleaned_data['technologies'])

			userquery = User.objects.filter(id=user)
			print(userquery)
			print(userquery[0])   #lelodie  not 1   arrive pas recuperer ce k je veux

			print(userquery.values('id')) #<QuerySet [{'id': 1}]> comt debarrasse de ce queryset
			bidouille = userquery.values_list('id', flat=True)
			bidouille = list(bidouille)
			print(bidouille)
			#format=[]
			#for index, truc in userquery:
			#	truc[index] = truc
			#	format.append(truc)
			#print(format)    => cannot unpack non-iterable User object
			#The QuerySet value for an exact lookup must be limited to one result using slicing.
			userProfilequery = UserProfile.objects.filter(user_id=int(bidouille[0])) #(user_id=int(bidouille[0]))
			print(userProfilequery)  # []
			tech = articles_form.cleaned_data['technologies']  #Technologies.objects.filter(name=articles_form.cleaned_data['technologies'])
			#Cannot assign "<QuerySet []>": "Articles.user" must be a "UserProfile" instance.
			newarticle = Articles.objects.create(user=userProfilequery.first(), title= articles_form.cleaned_data['title'], content= articles_form.cleaned_data['content'], picture= None)
			print(newarticle)
			newarticle.technologies.add(*tech)
			print(newarticle)
			newarticle.save()
			print(newarticle)
			if 'picture' in request.FILES:

				newarticle.picture = request.FILES['picture']
				newarticle.save()
				return redirect('articlesAdmin')
			else:
				newarticle.picture = "/article_pic/imageskkk.png"
				newarticle.save()
			return redirect('articlesAdmin')

	else:
		articles_form = ArticlesForm()
	return render(request, 'public/articlesForm.html', {'form': articles_form})






@login_required
def projectsCreation(request):

	if request.method == 'POST':
		project_form = ProjectsForm(request.POST)
		user = request.POST.get('user')

		if project_form.is_valid():
			#print(articles_form.cleaned_data)
			#print(articles_form.cleaned_data['technologies'])

			userquery = User.objects.filter(id=user)
			#print(userquery)
			#print(userquery[0])   #lelodie  not 1   arrive pas recuperer ce k je veux

			print(userquery.values('id')) #<QuerySet [{'id': 1}]> comt debarrasse de ce queryset
			bidouille = userquery.values_list('id', flat=True)
			bidouille = list(bidouille)
			print(bidouille)
			#format=[]
			#for index, truc in userquery:
			#	truc[index] = truc
			#	format.append(truc)
			#print(format)    => cannot unpack non-iterable User object
			#The QuerySet value for an exact lookup must be limited to one result using slicing.
			userProfilequery = UserProfile.objects.filter(user_id=int(bidouille[0])) #(user_id=int(bidouille[0]))
			print(userProfilequery)  # []
			tech = project_form.cleaned_data['technologies']  #Technologies.objects.filter(name=articles_form.cleaned_data['technologies'])
			#Cannot assign "<QuerySet []>": "Articles.user" must be a "UserProfile" instance.
			newproject = Projects.objects.create(user=userProfilequery.first(), name= project_form.cleaned_data['name'], description= project_form.cleaned_data['description'], illustration= None, repo_url= project_form.cleaned_data['repo_url'], website_url= project_form.cleaned_data['website_url'])
			print(newproject)
			newproject.technologies.add(*tech)
			print(newproject)
			newproject.save()
			print(newproject)
			if 'illustration' in request.FILES:

				newproject.illustration = request.FILES['illustration']
				newproject.save()
				return redirect('projectsAdmin')
			else:
				newproject.illustration = "project_illustration/imageskkk.png"
				newproject.save()

			return redirect('projectsAdmin')

	else:
		project_form = ProjectsForm()
	return render(request, 'public/projectsForm.html', {'form': project_form})



@login_required
def messages(request):
	messages_list= Messages.objects.all().order_by('read')

	context= {
		"messages" : messages_list
	}
	return render(request, 'public/messages.html', context)

@login_required
def messagesUpdate(request):
	if request.method =='POST':
		msg_read = request.POST.get('read')
		if msg_read =="on":
			msg_read=True
			msg_id = request.POST.get('id')
			msg_to_modify= Messages.objects.get(pk=int(msg_id))
			msg_to_modify.read = msg_read
			msg_to_modify.save()


			return HttpResponse('success')

@login_required
def articlesAdmin(request):
	articles_list= Articles.objects.all().order_by('created_at')

	context= {
		"articles" : articles_list
	}
	return render(request, 'public/articlesAdmin.html', context)

@login_required
def projectsAdmin(request):
	projects_list= Projects.objects.all().order_by('created_at')

	context= {
		"projects" : projects_list
	}
	return render(request, 'public/projectsAdmin.html', context)

@login_required
def articlesDelete(request, id):
	article_to_delete= Articles.objects.get(pk=int(id))
	article_to_delete.delete()
	return redirect('articlesAdmin')

@login_required
def projectsDelete(request, id):
	project_to_delete= Projects.objects.get(pk=int(id))
	project_to_delete.delete()
	return redirect('projectsAdmin')

@login_required
def technologiesAdmin(request):
	techno_list= Technologies.objects.all().order_by('name')

	context= {
		"technologies" : techno_list
	}
	return render(request, 'public/technologiesAdmin.html', context)

@login_required
def technologiesDelete(request, id):
	tech_to_delete = Technologies.objects.get(pk=int(id))
	tech_to_delete.delete()
	return redirect('technologiesAdmin')

@login_required
def technologiesUpdate(request, id):
	tech_to_update = Technologies.objects.get(pk=int(id))
	techno_form = TechnologiesForm(request.POST or None, instance= tech_to_update)
	if techno_form.is_valid():
		new_tech= techno_form.save()
		if 'picture' in request.FILES:

			new_tech.picture = request.FILES['picture']
			new_tech.save()
			return redirect('technologiesAdmin')
		return redirect('technologiesAdmin')
	return render (request, 'public/dashboard.html' ,{'form': techno_form})

@login_required
def projectsUpdate(request, id):
	data_to_update = Projects.objects.get(pk=int(id))
	project_form = ProjectsForm(request.POST or None, instance= data_to_update)
	if project_form.is_valid():
		new_project= project_form.save()
		if 'illustration' in request.FILES:

			new_project.illustration = request.FILES['illustration']
			new_project.save()
			return redirect('projectsAdmin')
		return redirect('projectsAdmin')
	return render (request, 'public/projectsForm.html' ,{'form': project_form})

@login_required
def articlesUpdate(request, id):
	data_to_update = Articles.objects.get(pk=int(id))
	article_form = ArticlesForm(request.POST or None, instance= data_to_update)
	if article_form.is_valid():
		new_article= article_form.save()
		if 'picture' in request.FILES:

			new_article.picture = request.FILES['picture']
			new_article.save()
			return redirect('articlesAdmin')
		return redirect('articlesAdmin')
	return render (request, 'public/articlesForm.html' ,{'form': article_form})

@login_required
def loginlanding(request):
	user = User.objects.get(pk=int(request.user.id))
	user_additional_info = UserProfile.objects.filter(user=int(request.user.id)).first()
	site = SiteSections.objects.all()

	context= {
		"user" :  user,
		"userAdditional" : user_additional_info,
		"site": site
	}
	return render (request, 'public/loginlanding.html' , context)


@login_required
def sectionsCreation(request):
	if request.method =='POST':
		sectionForm= SiteSectionsForm(request.POST)
		if sectionForm.is_valid:
			newSection = sectionForm.save()
			if 'picture' in request.FILES:
				newSection.picture = request.FILES['picture']
				newSection.save()
				return redirect('sectionForm')
	else:
		sectionForm= SiteSectionsForm()
		context={
			"form" : sectionForm
		}
		return render (request, 'public/sectionsForm.html', context)

@login_required
def usersUpdate(request, id):
	user_to_update = User.objects.get(pk=int(id))
	userProfile_to_update = UserProfile.objects.filter(user=int(id)).first()

	user_form = UserForm(request.POST or None, instance= user_to_update)
	user_profile_form = UserProfileForm(request.POST or None, instance= userProfile_to_update)

	if user_form.is_valid() and user_profile_form.is_valid():
		updated_user = user_form.save()
		updated_userProfile = user_profile_form.save()
		if 'avatar' in request.FILES:

			updated_userProfile.avatar = request.FILES['avatar']
			updated_userProfile.save()
			return redirect('loginlanding')
		return redirect('loginlanding')
	return render (request, 'public/registration.html' ,{'user_form': user_form, 'profile_form': user_profile_form})

@login_required
def logout(request):
	django_logout(request)
	return redirect('home')