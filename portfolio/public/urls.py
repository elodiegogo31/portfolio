from django.urls import path, re_path
from django.conf.urls.static import static
from django.conf import settings

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('home', views.home, name='home'),
    path('projects', views.projects, name='projectListing'),
    path('articles', views.articles, name='articleListing'),
    path('contact', views.contacts, name='contactForm'),

    path('register', views.register, name='register'),
    path('login', views.user_login, name='user_login'),

    path('private/technologies', views.dashboard, name='dashboard'),
    path('private/articles', views.articlesCreation, name='articleForm'),
    path('private/projects', views.projectsCreation, name='projectForm'),
    path('private/messages', views.messages, name='messagesListing'),
    path('private/messages/update', views.messagesUpdate, name='messagesUpdate'),
    path('private/articles/admin', views.articlesAdmin, name='articlesAdmin'),
    path('private/projects/admin', views.projectsAdmin, name='projectsAdmin'),
   # re_path(r'^private/articles/delete/(?P<id>[0-9]{0-4})/$', views.articlesDelete, name='articlesDelete'),
    path('private/articles/delete/<int:id>', views.articlesDelete, name='articlesDelete'),
    path('private/projects/delete/<int:id>', views.projectsDelete, name='projectsDelete'),
    path('private/technologies/delete/<int:id>', views.technologiesDelete, name='technologiesDelete'),
    path('private/technologies/admin', views.technologiesAdmin, name='technologiesAdmin'),

    path('private/technologies/update/<int:id>', views.technologiesUpdate, name='technologiesUpdate'),
    path('private/projects/update/<int:id>', views.projectsUpdate, name='projectsUpdate'),
    path('private/articles/update/<int:id>', views.articlesUpdate, name='articlesUpdate'),

    path('private/sections', views.sectionsCreation, name='sectionForm'),
    path('private/loginlanding', views.loginlanding, name='loginlanding'),
    path('private/users/update/<int:id>', views.usersUpdate, name="usersUpdate"),
    path('logout', views.logout, name='logout'),
]
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)