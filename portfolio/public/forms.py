from django import forms
from .models import UserProfile, Technologies, Articles, Projects, Messages, SiteSections
from django.contrib.auth.models import User

class UserForm(forms.ModelForm):
	password = forms.CharField(widget=forms.PasswordInput())
	class Meta():
		model = User
		fields = ('username', 'password', 'email', 'first_name', 'last_name')

class UserProfileForm(forms.ModelForm):
	class Meta():
		model = UserProfile
		fields = ('avatar', 'tel', 'address')


class TechnologiesForm(forms.ModelForm):
	class Meta():
		model = Technologies
		fields = ('name', 'picture')
	

class ArticlesForm(forms.ModelForm):
	class Meta():
		model = Articles
		fields = ('title','content','picture','technologies')

class ProjectsForm(forms.ModelForm):
	class Meta():
		model = Projects
		fields =('name', 'description', 'illustration', 'repo_url','website_url', 'technologies')

class MessagesForm(forms.ModelForm):
	class Meta():
		model = Messages
		fields =('lastname', 'firstname', 'content','email')

class SiteSectionsForm(forms.ModelForm):
	class Meta():
		model = SiteSections
		fields =('name', 'picture')