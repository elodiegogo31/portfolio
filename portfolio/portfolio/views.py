from django.shortcuts import render
from django.contrib.auth import authenticate, login


def login(request):
	username = request.POST['username']
	password = request.POST['password']
	user = authenticate(request, username=username, password=password)
	
	if user is not None:
		login(request, user)
		return render(request, 'public/login.html')
	else:
		return render(request, 'public/home.html')


	